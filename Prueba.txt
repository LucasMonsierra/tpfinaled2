LIST P=16F887	; list directive to define processor
#INCLUDE <P16F887.inc>	; processor specific variable definitions

; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	    __CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF &_CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF
	    __CONFIG    _INTRC_OSC_NOCLKOUT & _CONFIG2, _WRT_OFF & _BOR21V

;***** VARIABLE DEFINITIONS
AUX	    EQU		0x20		; variable used for context saving

;***** INICIO DE PROGRAMA
	    ORG		0x00
	    GOTO	INICIO
	    ORG	    	0x05

;***** CONFIGURACIÓN DE PUERTOS	
INICIO	    
	    
	    BSF		STATUS,RP1 ; NOS MOVEMOS AL BANCO 3
	    BSF		STATUS,RP0
	    MOVLW	0xF7	   ;Ponemos B en FF
	    MOVWF	TRISB	    
	    CLRF	ANSEL	
	    BANKSEL	TRISA
	    MOVLW	0x00
	    MOVWF	TRISA
	    BANKSEL	WPUB
	    BCF		OPTION_REG,7 ;Activo pull up
	    MOVLW	0xFF
	    MOVWF	WPUB
	    
;***** RUTINA DE LECTURA Y CARGA DE DATOS
SUMA
	    BANKSEL	PORTA
	    MOVF	PORTB
	    ANDLW	0x0F
	    MOVWF	AUX
	    MOVF	PORTB,0
	    MOVWF	0x21
	    SWAPF	0x21,0
	    ANDLW	0x0F
	    
;***** RUTINA DE SUMA
	    ADDWF	AUX
	    
;***** RUTINA DE VERIFICACIÓN DE CARRY DE 4 BITS
	    BTFSC	0x21,4
	    GOTO	CATCH
	    
;***** RUTINA DE ENVIO DE DATOS AL PUERTO DE SALIDA
   	    MOVWF	PORTA
	    GOTO	SUMA
;***** MANEJO DE INTERRUPCIÓN
CATCH	    
	    BSF		PORTA,4
	    ; LOOP DE RETARDO 1s
	    BCF		PORTA,4
	    ; LOOP DE RETARDO 1s
	    GOTO	CATCH
	    
;***** FIN DE PROGRAMA
	    END
	    
